﻿using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Zaymigo.Api.Models;

namespace Zaymigo.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationRequest(CreditApplicationRequestModel model);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationRequest(CreditApplicationRequestModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.ZaymigoStatus,
                model.ZaymigoCode,
                model.ZaymigoError,
                model.ZaymigoDataId,
                model.ZaymigoDataStatus,
                model.ZaymigoDataRejectionReason,
                model.ZaymigoDataValidationErrors
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    zaymigo_status,
                    zaymigo_code,
                    zaymigo_error,
                    zaymigo_data_id,
                    zaymigo_data_status,
                    zaymigo_data_rejection_reason,
                    zaymigo_data_validation_errors
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :zaymigoStatus,
                    :zaymigoCode,
                    :zaymigoError,
                    :zaymigoDataId,
                    :zaymigoDataStatus,
                    :zaymigoDataRejectionReason,
                    CAST(:zaymigoDataValidationErrors AS json)
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}