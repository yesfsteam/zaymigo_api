﻿namespace Zaymigo.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToZaymigoPhoneFormat(this string phoneNumber)
        {
            return string.IsNullOrWhiteSpace(phoneNumber) ? null : $"+{phoneNumber}";
        }
    }
}