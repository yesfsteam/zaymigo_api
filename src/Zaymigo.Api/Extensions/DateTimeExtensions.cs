﻿using System;

namespace Zaymigo.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToZaymigoDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("dd.MM.yyyy") : string.Empty;
        }
    }
}