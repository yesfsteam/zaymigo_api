﻿using System.Threading.Tasks;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.CreditApplication.Api.Contracts.Events;
using Zaymigo.Api.Domain;

namespace Zaymigo.Api.Messaging
{
    public class CreditApplicationProjection : Projection
    {
        private readonly ICreditApplicationManager manager;

        public CreditApplicationProjection(ICreditApplicationManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(CreditApplicationCreatedEvent @event)
            => await manager.CheckCreditApplication(@event);
    }
}