﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Yes.Infrastructure.Http;
using Zaymigo.Api.Models;

namespace Zaymigo.Api.Domain
{
    public interface IZaymigoClient
    {
        Task<Response<ZaymigoCreditApplicationResponse>> CreateCreditApplication(ZaymigoCreditApplicationRequest request);
    }

    public class ZaymigoClient : IZaymigoClient
    {
        private readonly HttpClient httpClient;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public ZaymigoClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
            jsonSerializerSettings = new JsonSerializerSettings{ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }

        public async Task<Response<ZaymigoCreditApplicationResponse>> CreateCreditApplication(ZaymigoCreditApplicationRequest request)
        {
            return await post<ZaymigoCreditApplicationResponse>(string.Empty, request);
        } 
        
        private async Task<Response<T>> post<T>(string uri, ZaymigoCreditApplicationRequest obj)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var content = new JsonContent(obj, jsonSerializerSettings);
            var response = await httpClient.PostAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }
    }
}