﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using Zaymigo.Api.Data;
using Zaymigo.Api.Extensions;
using Zaymigo.Api.Models;
using Zaymigo.Api.Models.Configuration;
using Zaymigo.Api.Models.Enums;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace Zaymigo.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
       // Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IZaymigoClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, IZaymigoClient client, ApplicationConfiguration configuration,
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.CreditAmount < configuration.MinCreditAmount || @event.CreditAmount > configuration.MaxCreditAmount)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, $"Сумма кредита должна быть от {configuration.MinCreditAmount} до {configuration.MaxCreditAmount} руб.");
                    logger.LogInformation($"CheckCreditApplication. Skip. Invalid credit amount. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                if (@event.ProfileType == ProfileType.Short)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Short profile. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                var request = createRequest(@event.CreditApplicationId, @event, beginTime);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    ZaymigoStatus = response.Content?.Status?.ToString(),
                    ZaymigoCode = (int?)response.Content?.Code,
                    ZaymigoError = response.Content?.Error,
                    ZaymigoDataId = response.Content?.Data?.Id,
                    ZaymigoDataStatus = response.Content?.Data?.Status?.ToString(),
                    ZaymigoDataRejectionReason = response.Content?.Data?.RejectionReason,
                    ZaymigoDataValidationErrors = response.Content?.Data?.validation_errors?.ToJsonString()
                });


                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content?.Status == ZaymigoRequestStatus.success)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Check request approved. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                    }
                    else
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Rejected, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Check request declined. Duration: {beginTime.GetDuration()} Request: {request}, Response: {response.Content}, Command: {command}");
                    }
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Check request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Check request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Check request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing check request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing check request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }
/*
        public async Task<Response<CreditApplicationResponse>> ResendCreditApplicationRequest(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            if (model.CreditAmount < configuration.MinCreditAmount || model.CreditAmount > configuration.MaxCreditAmount)
            {
                logger.LogInformation($"ResendCreditApplicationRequest. Skip. Invalid credit amount. Duration: {beginTime.GetDuration()} Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, $"Сумма кредита должна быть от {configuration.MinCreditAmount} до {configuration.MaxCreditAmount} руб.");
            }

            return createResponse(beginTime, CreditOrganizationRequestStatus.Approved);
        }
        */
        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, CreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.ProfileType == ProfileType.Short)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Short profile. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой анкеты");
                }
                
                var request = createRequest(creditApplicationId, model, beginTime);
                var response = await client.CreateCreditApplication(request);

                await repository.SaveCreditApplicationRequest(new CreditApplicationRequestModel
                {
                    CreditApplicationId = creditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    ZaymigoStatus = response.Content?.Status?.ToString(),
                    ZaymigoCode = (int?)response.Content?.Code,
                    ZaymigoError = response.Content?.Error,
                    ZaymigoDataId = response.Content?.Data?.Id,
                    ZaymigoDataStatus = response.Content?.Data?.Status?.ToString(),
                    ZaymigoDataRejectionReason = response.Content?.Data?.RejectionReason,
                    ZaymigoDataValidationErrors = response.Content?.Data?.validation_errors?.ToJsonString()
                });

                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    if (response.Content?.Status == ZaymigoRequestStatus.success)
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirmation request confirmed. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed);
                    }

                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request rejected. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, Response: {response.Content}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.Rejected, response.Content.ToJsonString());
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Confirmation request validation fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Confirmation request authorization fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Confirmation request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing confirmation request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private ZaymigoCreditApplicationRequest createRequest(Guid creditApplicationId, CreditApplicationRequest request, DateTime date)
        {
            var registrationAddress = new ZaymigoAddress
            {
                FullString = $"{request.RegistrationAddressRegion} {request.RegistrationAddressCity} {request.RegistrationAddressStreet} {request.RegistrationAddressHouse} {request.RegistrationAddressApartment}",
                Region = request.RegistrationAddressRegion,
                City = request.RegistrationAddressCity,
                Street = request.RegistrationAddressStreet,
                House = getHouseValue(request.RegistrationAddressHouse),
                Apartment = request.RegistrationAddressApartment
            };
            return new ZaymigoCreditApplicationRequest
            {
                Term = request.CreditPeriod > 30 ? 30 : request.CreditPeriod ?? 0,
                Amount = request.CreditAmount ?? 0,
                //Webmaster = Guid.NewGuid().ToString().Replace("-",string.Empty),
                Borrower = new ZaymigoBorrower
                {
                    MobilePhone = request.PhoneNumber.ToZaymigoPhoneFormat(),
                    FirstName = request.FirstName,
                    Surname = request.LastName,
                    MiddleName = request.MiddleName,
                    BirthDate = request.DateOfBirth.ToZaymigoDateFormat(),
                    BirthPlace = request.PlaceOfBirth,
                    //HomePhone =
                    WorkPhone = request.EmployerPhoneNumber.ToZaymigoPhoneFormat(),
                    Mail = request.Email,
                    Gender = request.Gender.HasValue ? (request.Gender == Gender.Male ? ZaymigoGender.male : ZaymigoGender.female).ToString() : null,
                    Passport = new ZaymigoPassport
                    {
                        Number = $"{request.PassportSeries} {request.PassportNumber}",
                        Issuer = request.PassportIssuer,
                        IssuerCode = request.PassportDepartmentCode,
                        IssueDate = request.PassportIssueDate.ToZaymigoDateFormat()
                    },
                    RegisterAddress = registrationAddress,
                    RealAddress = request.IsResidenceAddressEmpty()
                        ? registrationAddress
                        : new ZaymigoAddress
                        {
                            FullString = $"{request.ResidenceAddressRegion} {request.ResidenceAddressCity} {request.ResidenceAddressStreet} {request.ResidenceAddressHouse} {request.ResidenceAddressApartment}",
                            Region = request.ResidenceAddressRegion,
                            City = request.ResidenceAddressCity,
                            Street = request.ResidenceAddressStreet,
                            House = getHouseValue(request.ResidenceAddressHouse),
                            Apartment = request.ResidenceAddressApartment
                        }
                },
                ClickId = creditApplicationId.ToString()
            };
        }
        
        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }
        
        private string getHouseValue(string house)
        {
            if (string.IsNullOrWhiteSpace(house))
                return string.Empty;
            if (house.Length > 10)
                house = house.Replace(" ", string.Empty);
            return house.Length > 10 ? house.Substring(0, 10) : house;
        }
    }
}