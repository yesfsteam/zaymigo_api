﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Yes.Infrastructure.Common.Models;
using Zaymigo.Api.Models.Enums;

namespace Zaymigo.Api.Models
{
    public class ZaymigoCreditApplicationResponse : JsonModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ZaymigoRequestStatus? Status { get; set; }
        public ZaymigoApplicationStatusCode? Code { get; set; }
        public string Error { get; set; }
        public ZaymigoResponseData Data { get; set; }
    }
}