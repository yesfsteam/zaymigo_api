﻿using System;

namespace Zaymigo.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Guid CreditOrganizationId { get; set; }
        public int MinCreditAmount { get; set; }
        public int MaxCreditAmount { get; set; }
    }
}