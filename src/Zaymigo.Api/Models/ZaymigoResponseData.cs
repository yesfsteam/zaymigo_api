﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Zaymigo.Api.Models.Enums;

namespace Zaymigo.Api.Models
{
    public class ZaymigoResponseData
    {
        public int Id { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ZaymigoApplicationStatus? Status  { get; set; }
        public string RejectionReason { get; set; }
        // ReSharper disable once InconsistentNaming
        public Dictionary<string, string> validation_errors { get; set; }
    }
}