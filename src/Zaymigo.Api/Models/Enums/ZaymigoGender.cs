﻿// ReSharper disable InconsistentNaming
namespace Zaymigo.Api.Models.Enums
{
    public enum ZaymigoGender
    {
        male,
        female
    }
}