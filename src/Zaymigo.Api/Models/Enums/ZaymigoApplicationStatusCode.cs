﻿namespace Zaymigo.Api.Models.Enums
{
    public enum ZaymigoApplicationStatusCode
    {
        Success = 0,
        ValidationError = 1,
        UnexpectedError = 2
    }
}