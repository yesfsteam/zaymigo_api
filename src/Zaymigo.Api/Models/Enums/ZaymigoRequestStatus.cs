﻿// ReSharper disable InconsistentNaming
namespace Zaymigo.Api.Models.Enums
{
    public enum ZaymigoRequestStatus
    {
        success,
        error
    }
}