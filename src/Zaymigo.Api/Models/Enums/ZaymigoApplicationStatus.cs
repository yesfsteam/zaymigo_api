﻿// ReSharper disable InconsistentNaming
namespace Zaymigo.Api.Models.Enums
{
    public enum ZaymigoApplicationStatus
    {
        pending,
        success,
        error,
        rejected
    }
}