﻿namespace Zaymigo.Api.Models
{
    public class ZaymigoPassport
    {
        public string Number { get; set; }
        public string Issuer { get; set; }
        public string IssuerCode { get; set; }
        public string IssueDate { get; set; }
    }
}