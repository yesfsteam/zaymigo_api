﻿using Yes.Infrastructure.Common.Models;

namespace Zaymigo.Api.Models
{
    public class ZaymigoCreditApplicationRequest : JsonModel
    {
        public int Term { get; set; }
        public decimal Amount { get; set; }
        public ZaymigoBorrower Borrower { get; set; }
        //public string Webmaster { get; set; }
        public string ClickId { get; set; }
    }
}