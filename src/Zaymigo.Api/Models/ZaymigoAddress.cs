﻿namespace Zaymigo.Api.Models
{
    public class ZaymigoAddress
    {
        public string FullString { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        //public string Settlement { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        //public string Housing { get; set; }
        public string Apartment { get; set; }
    }
}