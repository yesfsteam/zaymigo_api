﻿using System;
using System.Net;
using Yes.Infrastructure.Common.Models;

namespace Zaymigo.Api.Models
{
    public class CreditApplicationRequestModel : JsonModel
    {
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Статус запроса
        /// </summary>
        public string ZaymigoStatus { get; set; }
        
        /// <summary>
        /// Код статуса запроса
        /// </summary>
        public int? ZaymigoCode { get; set; }
        
        /// <summary>
        /// Общее текстовое описание ошибки
        /// </summary>
        public string ZaymigoError { get; set; }
        
        /// <summary>
        /// Уникальный идентификатор заявки
        /// </summary>
        public int? ZaymigoDataId { get; set; }
        
        /// <summary>
        /// Статус обработки заявки
        /// </summary>
        public string ZaymigoDataStatus { get; set; }
        
        /// <summary>
        /// Поле с описанием причины отказа приёма
        /// </summary>
        public string ZaymigoDataRejectionReason { get; set; }
        
        /// <summary>
        /// Массив с ошибками валидации
        /// </summary>
        public string ZaymigoDataValidationErrors { get; set; }
    }
}