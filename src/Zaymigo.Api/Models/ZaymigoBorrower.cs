﻿namespace Zaymigo.Api.Models
{
    public class ZaymigoBorrower
    {
        public string MobilePhone { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string Mail { get; set; }
        public string Gender { get; set; }
        public ZaymigoPassport Passport { get; set; }
        public ZaymigoAddress RegisterAddress { get; set; }
        public ZaymigoAddress RealAddress { get; set; }
    }
}